# Copyright Michele Vidotto 2014 <michele.vidotto@gmail.com>

# TODO:
#

# this function install all the links at once
.PHONY: link_install
link_install:
	@echo installing links ... \
	$(foreach KEY,$(HASHES), \
		$(shell ln -sf $($(KEY)) $(KEY)) \
	)


STATS = $(addsuffix .stats,$(COMPARE))
%.stats:
	!threads
	$(call module_loader); \
	arr=($(call split,.,$*)); \   * the larger K-mer hash must be provided first *
	arr2=("$${arr[@]}"); \   * copy array to another *
	if (( "$$(stat -Lc %s $${arr[0]})" < "$$(stat -Lc %s $${arr[1]})" )); then \
		arr2[1]=$${arr[0]}; \   * switch names if the second is the larger K-mer hash *
		arr2[0]=$${arr[1]}; \
	fi; \
	echo -e "\nfirst := $${arr[0]}\n\nsecond := $${arr[1]}\n"; \
	kat comp \
	--threads=$$THREADNUM \
	--verbose \
	--output_type=png \
	--output_prefix=$(basename $@) \
	$${arr2[0]} $${arr2[1]}

MX = $(STATS:.stats=-main.mx)
.PRECIOUS: $(MX)
%-main.mx: %.stats
	touch $@


CN_SPECTRA = $(MX:-main.mx=.cn.spectra.png)
%.cn.spectra.png: %-main.mx
	$(call module_loader); \
	kat plot spectra-cn \
	--x_max=$(CN_PLOT_X_LIM) \
	--y_max=$(CN_PLOT_Y_LIM) \
	--width=10 \
	--height=10 \
	--title="Spectra CN plot for: $(call merge, Vs ,$(call split,.,$*))" \
	--output=$@ \
	$<


SHARED_SPECTRA = $(MX:-main.mx=.shared.spectra.png)
%.shared.spectra.png: %-main.mx
	$(call plot_modules); \
	kat plot spectra-mx \
	--intersection \
	--x_max $(SHARED_PLOT_X_LIM) \
	--y_max $(SHARED_PLOT_Y_LIM) \
	--output_type=png \
	--verbose \
	--output=$@ \
	$<


DENSITY = $(MX:-main.mx=.density.png)
%.density.png: %-main.mx
	$(call module_loader); \
	kat plot density \
	--y_max=$(DENSITY_PLOT_Y_LIM) \
	--x_max=$(DENSITY_PLOT_X_LIM) \
	--z_max=$(DENSITY_PLOT_Z_MAX) \
	--width=15 \
	--height=10 \
	--not_rasterised \
	--title="Density plot for: $(call merge, Vs ,$(call split,.,$*))" \
	--output $@ \
	$<


DIST_ANALYSIS = $(MX:-main.mx=.dist.analysis.log)
%.dist.analysis.log: %-main.mx
	$(call module_loader); \
	dist_analysis.py $< 4 $(CN_PLOT_X_LIM) 5 500000 \
	| tee $@


.PHONY: test
test:
	@echo $(SHARED_SPECTRA)


ALL +=	link_install \
	$(STATS) \
	$(MX) \
	$(CN_SPECTRA) \
	$(SHARED_SPECTRA) \
	$(DENSITY) \
	#$(DIST_ANALYSIS)

INTERMEDIATE += 

CLEAN += clean_link

# add dipendence to clean targhet
clean: clean_link

.PHONY: clean_link
clean_link:
	$(foreach KEY,$(HASHES), $(shell rm -f $(KEY)) )
